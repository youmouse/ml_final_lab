import lime
from modules.models.algorithms import get_algorithm 
from modules.models.algorithm_interpretation import lime_class
from modules.utils.io import get_data
from modules.visualization.graphs import tree_graph
import pandas as pd
import os

def main():
	project_route =os.getcwd()
	print(" ML - Final Lab")
	print("================")


	# Get data 
	X_names, y_names,X_train, X_test, y_train, y_test = get_data("poker")
	
	# Select interpretable algorithm
	interpretable_model = get_algorithm("decission_tree_classifier")
	
	# Train algorithm 
	ml_tree = interpretable_model.fit(X_train,y_train)
	
	# Visualize results
	tree_graph(ml_tree,X_names)
	
	# Select NON-interpretable algorithm
	non_interpretable_model = get_algorithm("random_forest_classifier")
	non_interpretable_model.fit(X_train,y_train)

	# Interpret the algorithm
	# https://marcotcr.github.io/lime/tutorials/Tutorial%20-%20continuous%20and%20categorical%20features.html
	#explainer =lime(X_train,X_test,X_names, y_names,num_features=2,non_interpretable_model)
	limer = lime_class(X_train,X_names, y_names)
	limer.explain_instance(X_test,non_interpretable_model)
	# Visualize results
	#sklearn.metrics.accuracy_score(y_test, model.predict(X_test))


if __name__ == "__main__" :
	pd.set_option('display.width', 800)
	pd.set_option('display.max_columns', 30)
	#get_data("poker")
	main()
	"""


if __name__ == '__main__':
	pd.set_option('display.width', 800)
	pd.set_option('display.max_columns', 30)
	project_route =os.getcwd() 

	# GET data cleaned  
	X,y,test_data  = datr.get_data(project_route)

	X_train, X_test, y_train, y_test=train_test_split(X, y, test_size=0.2, random_state=42)
	
	model =models.run_algorithm("decission_tree_classifier")
	model.fit(X_train,y_train)
	
	score = models.validator_score(model, X_train, y_train)
	print(score)
	
	score = models.validator_score(model, X_test, y_test)
	print(score)

	output_data = models.apply_model_to_test(model,test_data)

	models.save_output_data(project_route,output_data)
	"""