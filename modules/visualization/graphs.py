import pydotplus
from sklearn import tree
from os import getcwd, sep


def tree_graph(graph_tree,data_feature_names):	 
	images_path= getcwd()+sep+"images"+sep+"tree_graphs"+sep

	# Visualize data
	dot_data = tree.export_graphviz(graph_tree,
	                                feature_names=data_feature_names,
	                                out_file=None,
	                                filled=True,
	                                rounded=True)
	graph = pydotplus.graph_from_dot_data(dot_data)
	graph.write_png(images_path+'tree.png')

def tree_graph_notebook(graph_tree,data_feature_names):	 
	# Visualize data
	dot_data = tree.export_graphviz(graph_tree,
	                                feature_names=data_feature_names,
	                                out_file=None,
	                                filled=True,
	                                rounded=True)
	graph = pydotplus.graph_from_dot_data(dot_data)
	return graph.create_png()