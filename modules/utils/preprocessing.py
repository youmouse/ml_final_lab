import sklearn

def encoder_data_OHE(data,train,categorical_features = [1,2]):
	categorical_names = {}
	for feature in categorical_features:
	    le = sklearn.preprocessing.LabelEncoder()
	    le.fit(data[:, feature])
	    data[:, feature] = le.transform(data[:, feature])
	    categorical_names[feature] = le.classes_

	data = data.astype(float)
	encoder = sklearn.preprocessing.OneHotEncoder(categorical_features=categorical_features)
	encoder.fit(data)
	return encoder
	"""
	USAGE:
	encoder= encoder_data_OHE(data,X_train,categorical_features )

	encoder.fit(data)
	encoded_train = encoder.transform(train)
	model.fit(encoded_train, Y_train)
	"""