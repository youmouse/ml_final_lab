from sklearn import datasets
from sklearn.model_selection import train_test_split


import pandas as pd
import io
import requests



def Xy_iris():
	# import some data to play with
	iris = datasets.load_iris()
	#X = iris.data[:, :2]  # we only take the first two features.
	print(iris)
	X_train, X_test, y_train, y_test= train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)
	return iris.feature_names, iris.target_names, X_train, X_test, y_train, y_test
	
def Xy_poker():
	url_training="http://archive.ics.uci.edu/ml/machine-learning-databases/poker/poker-hand-training-true.data"
	url_testing= "http://archive.ics.uci.edu/ml/machine-learning-databases/poker/poker-hand-testing.data"
	
	headers=["S1","R1","S2","R2","S3","R3","S4","R4","S5","R5","CLASS"]
	
	req_training=requests.get(url_training).content
	req_testing=requests.get(url_testing).content
	
	training_data= pd.read_csv(io.StringIO(req_training.decode('utf-8')),names=headers)
	testing_data = pd.read_csv(io.StringIO(req_testing.decode('utf-8')),names=headers)
	#print(training_data)
	X_train = training_data[headers[:-1]]
	y_train = training_data[headers[-1]]

	X_test = testing_data[headers[:-1]]
	y_test = testing_data[headers[-1]]
	
	return headers[:-1], headers[-1], X_train, X_test, y_train, y_test

def get_data(arg):
	switcher ={
		"iris" : Xy_iris,
		"poker" : Xy_poker
	}
	return switcher.get(arg, lambda: "Invalid algoritm")()