from sklearn import tree
from sklearn.ensemble import RandomForestClassifier

def decission_tree_classifier():
	criterion = "entropy" #gini
	splitter = "random"   #best
	max_depth =15
	random_state = 25	  #0
	clf = tree.DecisionTreeClassifier(max_depth =max_depth ,criterion=criterion,splitter=splitter,random_state=random_state)
	#clf.fit(features,label)
	return clf
def random_forest_classifier():
	criterion = "entropy" #gini
	max_depth =15
	random_state = 23	  #0
	clf = RandomForestClassifier(max_depth =max_depth ,criterion=criterion,random_state=random_state)
	#clf.fit(features,label)
	return clf


def get_algorithm(arg):
	switcher ={
		"decission_tree_classifier" :decission_tree_classifier,
		"random_forest_classifier" :random_forest_classifier
	}
	return switcher.get(arg, lambda: "Invalid algoritm")()
