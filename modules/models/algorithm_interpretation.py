import lime
from lime import lime_tabular
import numpy as np


class lime_class:
	#explainer={}
	#X_names=[]
	def __init__(self,X_train,X_names, y_names):
		self.X_names=X_names
		self.explainer =lime_tabular.LimeTabularExplainer(X_train, 
												feature_names=X_names, 
												class_names=y_names, 
												discretize_continuous=True)
	def explain_instance(self,X_test, model):
		#np.random.seed(1)
		i = 16
		exp = self.explainer.explain_instance(X_test[i], 
										model.predict_proba, 
										num_features=2,#len(self.X_names), 
										top_labels=1)
		#self.exp = exp
		return exp

	def print_in_notebook(exp):
		return exp.show_in_notebook(show_table=True,show_all=True)

"""
def interpret_with(arg):
	switcher ={
		"LIME" :lime
	}
	return switcher.get(arg, lambda: "Invalid algoritm")()
"""